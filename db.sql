--
-- PostgreSQL database dump
--

-- Dumped from database version 14.1
-- Dumped by pg_dump version 14.1

-- Started on 2022-01-13 01:36:10 CET

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 212 (class 1259 OID 16406)
-- Name: categorie; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.categorie (
    id_categorie integer NOT NULL,
    sport character varying(50)
);


ALTER TABLE public.categorie OWNER TO admin;

--
-- TOC entry 220 (class 1259 OID 16524)
-- Name: categorie_id_categorie_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.categorie_id_categorie_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.categorie_id_categorie_seq OWNER TO admin;

--
-- TOC entry 3633 (class 0 OID 0)
-- Dependencies: 220
-- Name: categorie_id_categorie_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE public.categorie_id_categorie_seq OWNED BY public.categorie.id_categorie;


--
-- TOC entry 210 (class 1259 OID 16400)
-- Name: equipe; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.equipe (
    id_equipe integer NOT NULL,
    id_categorie integer NOT NULL,
    id_pays integer NOT NULL,
    nom character varying(50)
);


ALTER TABLE public.equipe OWNER TO admin;

--
-- TOC entry 217 (class 1259 OID 16444)
-- Name: equipe_id_categorie_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.equipe_id_categorie_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.equipe_id_categorie_seq OWNER TO admin;

--
-- TOC entry 3634 (class 0 OID 0)
-- Dependencies: 217
-- Name: equipe_id_categorie_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE public.equipe_id_categorie_seq OWNED BY public.equipe.id_categorie;


--
-- TOC entry 216 (class 1259 OID 16437)
-- Name: equipe_id_equipe_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.equipe_id_equipe_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.equipe_id_equipe_seq OWNER TO admin;

--
-- TOC entry 3635 (class 0 OID 0)
-- Dependencies: 216
-- Name: equipe_id_equipe_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE public.equipe_id_equipe_seq OWNED BY public.equipe.id_equipe;


--
-- TOC entry 218 (class 1259 OID 16451)
-- Name: equipe_id_pays_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.equipe_id_pays_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.equipe_id_pays_seq OWNER TO admin;

--
-- TOC entry 3636 (class 0 OID 0)
-- Dependencies: 218
-- Name: equipe_id_pays_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE public.equipe_id_pays_seq OWNED BY public.equipe.id_pays;


--
-- TOC entry 209 (class 1259 OID 16397)
-- Name: joueur; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.joueur (
    id_equipe integer NOT NULL,
    id_pays integer NOT NULL,
    id_categorie integer NOT NULL,
    nom character varying(50),
    prenom character varying(50),
    mail character varying(100),
    mdp character varying(50),
    id integer NOT NULL
);


ALTER TABLE public.joueur OWNER TO admin;

--
-- TOC entry 215 (class 1259 OID 16430)
-- Name: joueur_id_categorie_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.joueur_id_categorie_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.joueur_id_categorie_seq OWNER TO admin;

--
-- TOC entry 3637 (class 0 OID 0)
-- Dependencies: 215
-- Name: joueur_id_categorie_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE public.joueur_id_categorie_seq OWNED BY public.joueur.id_categorie;


--
-- TOC entry 213 (class 1259 OID 16416)
-- Name: joueur_id_equipe_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.joueur_id_equipe_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.joueur_id_equipe_seq OWNER TO admin;

--
-- TOC entry 3638 (class 0 OID 0)
-- Dependencies: 213
-- Name: joueur_id_equipe_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE public.joueur_id_equipe_seq OWNED BY public.joueur.id_equipe;


--
-- TOC entry 214 (class 1259 OID 16423)
-- Name: joueur_id_pays_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.joueur_id_pays_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.joueur_id_pays_seq OWNER TO admin;

--
-- TOC entry 3639 (class 0 OID 0)
-- Dependencies: 214
-- Name: joueur_id_pays_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE public.joueur_id_pays_seq OWNED BY public.joueur.id_pays;


--
-- TOC entry 221 (class 1259 OID 16531)
-- Name: joueur_id_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.joueur_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.joueur_id_seq OWNER TO admin;

--
-- TOC entry 3640 (class 0 OID 0)
-- Dependencies: 221
-- Name: joueur_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE public.joueur_id_seq OWNED BY public.joueur.id;


--
-- TOC entry 211 (class 1259 OID 16403)
-- Name: pays; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.pays (
    code integer NOT NULL,
    alpha3 character(3),
    nom character varying(50),
    fanion character varying(100)
);


ALTER TABLE public.pays OWNER TO admin;

--
-- TOC entry 219 (class 1259 OID 16466)
-- Name: pays_id_pays_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.pays_id_pays_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pays_id_pays_seq OWNER TO admin;

--
-- TOC entry 3641 (class 0 OID 0)
-- Dependencies: 219
-- Name: pays_id_pays_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE public.pays_id_pays_seq OWNED BY public.pays.code;


--
-- TOC entry 3458 (class 2604 OID 16525)
-- Name: categorie id_categorie; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.categorie ALTER COLUMN id_categorie SET DEFAULT nextval('public.categorie_id_categorie_seq'::regclass);


--
-- TOC entry 3454 (class 2604 OID 16438)
-- Name: equipe id_equipe; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.equipe ALTER COLUMN id_equipe SET DEFAULT nextval('public.equipe_id_equipe_seq'::regclass);


--
-- TOC entry 3455 (class 2604 OID 16445)
-- Name: equipe id_categorie; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.equipe ALTER COLUMN id_categorie SET DEFAULT nextval('public.equipe_id_categorie_seq'::regclass);


--
-- TOC entry 3456 (class 2604 OID 16452)
-- Name: equipe id_pays; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.equipe ALTER COLUMN id_pays SET DEFAULT nextval('public.equipe_id_pays_seq'::regclass);


--
-- TOC entry 3450 (class 2604 OID 16417)
-- Name: joueur id_equipe; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.joueur ALTER COLUMN id_equipe SET DEFAULT nextval('public.joueur_id_equipe_seq'::regclass);


--
-- TOC entry 3451 (class 2604 OID 16424)
-- Name: joueur id_pays; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.joueur ALTER COLUMN id_pays SET DEFAULT nextval('public.joueur_id_pays_seq'::regclass);


--
-- TOC entry 3452 (class 2604 OID 16431)
-- Name: joueur id_categorie; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.joueur ALTER COLUMN id_categorie SET DEFAULT nextval('public.joueur_id_categorie_seq'::regclass);


--
-- TOC entry 3453 (class 2604 OID 16532)
-- Name: joueur id; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.joueur ALTER COLUMN id SET DEFAULT nextval('public.joueur_id_seq'::regclass);


--
-- TOC entry 3457 (class 2604 OID 16467)
-- Name: pays code; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.pays ALTER COLUMN code SET DEFAULT nextval('public.pays_id_pays_seq'::regclass);


--
-- TOC entry 3617 (class 0 OID 16406)
-- Dependencies: 212
-- Data for Name: categorie; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.categorie (id_categorie, sport) FROM stdin;
1	Football
2	Basketball
3	Tennis
4	Cyclisme
5	Handball
12	Badminton
13	Boxe Anglaise
14	Rugby
15	Athlétisme
\.


--
-- TOC entry 3615 (class 0 OID 16400)
-- Dependencies: 210
-- Data for Name: equipe; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.equipe (id_equipe, id_categorie, id_pays, nom) FROM stdin;
2	1	250	Paris Saint Germain
1	1	826	Manchester United
3	2	840	Golden State Warriors
4	2	840	Utah Jazz
5	2	840	Los Angeles Clippers
\.


--
-- TOC entry 3614 (class 0 OID 16397)
-- Dependencies: 209
-- Data for Name: joueur; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.joueur (id_equipe, id_pays, id_categorie, nom, prenom, mail, mdp, id) FROM stdin;
2	250	1	Mbappe	Kylian	kmbappe@psg.fr	1234	2
1	250	1	Varane	Raphael	rvarane@mutd.co.uk	1234	3
3	840	2	Curry	Stephen	\N	\N	4
5	840	2	Leonard	Kawhi	\N	\N	5
4	250	2	Gobert	Rudy	\N	\N	10
\.


--
-- TOC entry 3616 (class 0 OID 16403)
-- Dependencies: 211
-- Data for Name: pays; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.pays (code, alpha3, nom, fanion) FROM stdin;
250	FRA	France	fr.png
826	GBR	Royaume-Uni	uk.png
840	USA	Etats-Unis	us.png
724	ESP	Espagne	es.png
\.


--
-- TOC entry 3642 (class 0 OID 0)
-- Dependencies: 220
-- Name: categorie_id_categorie_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('public.categorie_id_categorie_seq', 15, true);


--
-- TOC entry 3643 (class 0 OID 0)
-- Dependencies: 217
-- Name: equipe_id_categorie_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('public.equipe_id_categorie_seq', 1, false);


--
-- TOC entry 3644 (class 0 OID 0)
-- Dependencies: 216
-- Name: equipe_id_equipe_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('public.equipe_id_equipe_seq', 5, true);


--
-- TOC entry 3645 (class 0 OID 0)
-- Dependencies: 218
-- Name: equipe_id_pays_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('public.equipe_id_pays_seq', 1, true);


--
-- TOC entry 3646 (class 0 OID 0)
-- Dependencies: 215
-- Name: joueur_id_categorie_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('public.joueur_id_categorie_seq', 1, false);


--
-- TOC entry 3647 (class 0 OID 0)
-- Dependencies: 213
-- Name: joueur_id_equipe_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('public.joueur_id_equipe_seq', 6, true);


--
-- TOC entry 3648 (class 0 OID 0)
-- Dependencies: 214
-- Name: joueur_id_pays_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('public.joueur_id_pays_seq', 3, true);


--
-- TOC entry 3649 (class 0 OID 0)
-- Dependencies: 221
-- Name: joueur_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('public.joueur_id_seq', 10, true);


--
-- TOC entry 3650 (class 0 OID 0)
-- Dependencies: 219
-- Name: pays_id_pays_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('public.pays_id_pays_seq', 1, false);


--
-- TOC entry 3469 (class 2606 OID 16530)
-- Name: categorie id_categorie; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.categorie
    ADD CONSTRAINT id_categorie PRIMARY KEY (id_categorie);


--
-- TOC entry 3465 (class 2606 OID 16476)
-- Name: equipe id_equipe; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.equipe
    ADD CONSTRAINT id_equipe PRIMARY KEY (id_equipe);


--
-- TOC entry 3467 (class 2606 OID 16480)
-- Name: pays id_pays; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.pays
    ADD CONSTRAINT id_pays PRIMARY KEY (code);


--
-- TOC entry 3461 (class 2606 OID 16537)
-- Name: joueur joueur_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.joueur
    ADD CONSTRAINT joueur_pkey PRIMARY KEY (id);


--
-- TOC entry 3462 (class 1259 OID 16549)
-- Name: fki_id_categorie; Type: INDEX; Schema: public; Owner: admin
--

CREATE INDEX fki_id_categorie ON public.equipe USING btree (id_categorie);


--
-- TOC entry 3459 (class 1259 OID 16555)
-- Name: fki_id_equipe; Type: INDEX; Schema: public; Owner: admin
--

CREATE INDEX fki_id_equipe ON public.joueur USING btree (id_equipe);


--
-- TOC entry 3463 (class 1259 OID 16543)
-- Name: fki_id_pays; Type: INDEX; Schema: public; Owner: admin
--

CREATE INDEX fki_id_pays ON public.equipe USING btree (id_pays);


--
-- TOC entry 3474 (class 2606 OID 16544)
-- Name: equipe id_categorie; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.equipe
    ADD CONSTRAINT id_categorie FOREIGN KEY (id_categorie) REFERENCES public.categorie(id_categorie) NOT VALID;


--
-- TOC entry 3472 (class 2606 OID 16561)
-- Name: joueur id_categorie; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.joueur
    ADD CONSTRAINT id_categorie FOREIGN KEY (id_categorie) REFERENCES public.categorie(id_categorie) NOT VALID;


--
-- TOC entry 3470 (class 2606 OID 16550)
-- Name: joueur id_equipe; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.joueur
    ADD CONSTRAINT id_equipe FOREIGN KEY (id_equipe) REFERENCES public.equipe(id_equipe) NOT VALID;


--
-- TOC entry 3473 (class 2606 OID 16538)
-- Name: equipe id_pays; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.equipe
    ADD CONSTRAINT id_pays FOREIGN KEY (id_pays) REFERENCES public.pays(code) NOT VALID;


--
-- TOC entry 3471 (class 2606 OID 16556)
-- Name: joueur id_pays; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.joueur
    ADD CONSTRAINT id_pays FOREIGN KEY (id_pays) REFERENCES public.pays(code) NOT VALID;


--
-- TOC entry 3632 (class 0 OID 0)
-- Dependencies: 3
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: admin
--

REVOKE ALL ON SCHEMA public FROM postgres;
REVOKE ALL ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO admin;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2022-01-13 01:36:10 CET

--
-- PostgreSQL database dump complete
--

