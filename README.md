# PROJET RUST : API WEB 

Projet Web par Axel LABARRE & Vincent VILFEU 

L2-Informatique - Université Paris VIII 

## Projet Web

Une application client serveur web avec une base de donnée ou pas. Un client applicatif riche ou pas.

<img src="https://i.ibb.co/HhfTpCM/Capture-d-e-cran-2022-01-12-a-04-16-14.png" alt="Capture-d-e-cran-2022-01-12-a-04-16-14" border="0">

### Server / Backend

- Rouille -> https://github.com/tomaka/rouille
- Database -> Postgresql

### Client / FrontEnd

- HTML 
- JavaScript 
- Template Boostrap css 

## Commandes Utiles 

> CMD + SHIFT + R  : Refresh without cache 


## Utilisation  

> cargo build

> cargo run 

> (En local) http://localhost:8000


## Screenshots 

Table Joueur, données récupérées via une requete GET vers l'api en JSON  récupérer puis parsé dans un tableau en javascript (src/app/js/script.js)
<img src="https://i.ibb.co/nR4JKgZ/Capture-d-e-cran-2022-01-13-a-00-32-23.png" alt="Capture-d-e-cran-2022-01-13-a-00-32-23" border="0">

## Postrgresql

En outre Export de la DB Postgres à la racine du dépot. 


## Conclusion

Ce projet web a été réalisé par étapes : 

- Création de la Base de donnée Postgresql et des relations.

- Création du serveur et connexion à la base de donnée. 

- Mise en place du routage et des requetes GET, POST , PUT , DELETE et Réponses JSON

- Implémentation des modèles de structures et la Serialisation avec SERDE

- Tests des requetes et du format JSON 

- Mise en place d'une partie Front-end malgré le peu de documentation sur le framework Rouille. 

- Utilisation des websockets pour manipuler les requetes permettant le chargement d'une page Html et des ressources. 

- Exploitation des données JSON de l'api rust pour les tables html à l'aide de l'API Fetch de javascript. 



Dans la mesure où en + de créer une api en rust , nous avons fais l'effort de proposer une app front-end via les Websockets.

