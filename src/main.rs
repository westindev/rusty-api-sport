mod model;
mod routes;


#[macro_use]
extern crate rouille;
extern crate postgres;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate curl;
extern crate serde_json;


/* POSTGRES */
use postgres::{Client, NoTls};
use std::sync::Mutex;




fn main() {
    /* Connexion a la base de donnée */
    /* postgres://root@localhost:3306/api_rust */
    let db = {
        let db = Client::connect(
            "postgres://postgres:password@localhost:5432/api_rust",
            NoTls,
        );
        Mutex::new(db.expect("Erreur de connexion à la base de donnée"))
    };

    println!("Serveur d'écoute : localhost:8000");

    /* Le mot-clé `move` sert a garandir le déplacement de la variable db dans la closure */
    rouille::start_server("localhost:8000", move |request| {
        //Emballage de la base donné et verrouillage avant utilisation.

        /* le verouillage nous donne un acces exclusif a la connexion a la base de donnée pour manipuler nos requete
            Ce qui malheureusment nous oblige a faire les requetes les unes après les autres.
        */
        let mut db = db.lock().unwrap();

        /* Démarrer une transaction alors qu'un panic est survenu entrainera un retour en arrière */
        let mut db = db.transaction().unwrap();

        //let api_response = api::api_routes(request, &mut db);

        /* Pour une meilleur lisibilité la requete sera manipulé dans une fonction séparé */
        let response = routes::routes(&request, &mut db);

        /* Si la reponse est un succès la transaction sera effectué */
        if response.is_success() {
            db.commit().unwrap();
        }
        response
        
    });
}
