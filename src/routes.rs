extern crate rouille;

use postgres::Transaction;
use rouille::Request;
use rouille::Response;

use crate::model::Joueur;
use crate::model::Sport;
use crate::model::Equipe;

pub fn handler(request: &rouille::Request) -> Response {
    rouille::match_assets(&request.remove_prefix("/app").unwrap(), "./src/app/")
}

/* La fonction qui manipule actuellement la requete  */
pub fn routes(request: &Request, db: &mut Transaction) -> Response {
   
    /* Ressources statiques pour application */
    let _url = "index.html";
    let _css = "styles.css";
    let _js = "script.js";
    let _chart_bar = "chart-area-demo.js";
    router!(request,
        /* Requete link pour les besoins du theme boostrap  */
        (GET) (/app/{_url:String}) => {handler(&request)},

        (GET) (/app/css/{_css:String}) => {handler(&request)},

        (GET) (/app/js/{_js:String}) => {handler(&request)},

        (GET) (/app/assets/demo/{_chart_bar:String}) => {handler(&request)},

        /* Racine du site */
        (GET) (/) => {
            /* Redirection vers la home page de l'application front end */
            Response::redirect_302("/app/index.html")
        },
        /* API  REQUETES */
        (GET) (/api/sports) => {
            /* Nouveau vecteur */
            let mut out = Vec::new();
            /* Requete utilisé pour itérer sur tout les lignes de la tables */

            if let Ok(rows) = &db.query("SELECT cat.id_categorie, cat.sport
                                        FROM categorie cat 
                                        GROUP BY cat.id_categorie
                                        ORDER BY cat.sport", &[]) {
            for row in rows {
                    let sport = Sport {
                        id: row.get(0),
                        sport: row.get(1),
                    };
                    out.push(sport);
                }
            }
            Response::json(&out)
        },

        (GET) (/api/joueurs) => {
            /* Nouveau vecteur */
            let mut out = Vec::new();
            /* Requete utilisé pour itérer sur tout les lignes de la tables */
            if let Ok(rows) = &db.query("SELECT jo.id, jo.nom, jo.prenom, eq.nom, cat.sport, pays.nom
                                        FROM joueur jo
                                        JOIN equipe eq
                                        ON eq.id_equipe = jo.id_equipe
                                        JOIN categorie cat
                                        ON jo.id_categorie = cat.id_categorie
                                        JOIN pays
                                        ON pays.code = jo.id_pays", &[]) {
                for row in rows {
                    let joueur = Joueur {
                        id_joueur: row.get(0),
                        nom: row.get(1),
                        prenom: row.get(2),
                        equipe: row.get(3),
                        sport: row.get(4),
                        pays: row.get(5),
                    };
                    out.push(joueur);
                }
            }
            Response::json(&out)
        },

        (GET) (/api/equipes) => {

            /* Nouveau vecteur */
            let mut out = Vec::new();
            /* Requete utilisé pour itérer sur tout les lignes de la tables */
            if let Ok(rows) = &db.query("SELECT eq.id_equipe, eq.nom, cat.sport, pays.nom
                                        FROM equipe AS eq 
                                        JOIN categorie cat 
                                        ON eq.id_categorie = cat.id_categorie 
                                        JOIN pays 
                                        ON pays.code = eq.id_pays", &[]) {
                for row in rows {
                    let equipe = Equipe {
                        id_equipe: row.get(0),
                        team_name: row.get(1),
                        sport: row.get(2),
                        country_name: row.get(3),
                    };
                    out.push(equipe);
                }
            }
            //let response = Response::json(&out);
            Response::json(&out)
        },

        (GET) (/api/equipe/{id: i32}) => {
            #[derive(Serialize)]
            struct Equipe {
                id: i32,
                team_name: String,
                pays: String,
            }

            /* Nouveau vecteur */
            let mut out = Vec::new();
            /* Requete utilisé pour itérer sur tout les lignes de la tables */
            if let Ok(rows) = &db.query("SELECT eq.id_equipe, eq.nom, pa.nom
                                        FROM equipe eq 
                                        JOIN pays pa 
                                        ON pa.code = eq.id_pays 
                                        WHERE eq.id_equipe = $1", &[&id]) {
                for row in rows {
                    let equipe = Equipe {
                        id: row.get(0),
                        team_name: row.get(1),
                        pays:row.get(2),
                    };
                    out.push(equipe);
                }
            }

            Response::json(&out)
        },

        (GET) (/api/joueur/{id: i32}) => {
            /* Cette route retourne les infos du joueurs si ils existent */

            #[derive(Serialize)]
            struct Joueur {
                name: String,
                lastname: String,
                mail: String,
                flag: String,
                sport: String,
            }

            /* Nouveau vecteur */
            let mut out = Vec::new();
            //* Cette requete écrit les infos dans `content` si la requete SQL est mal faites = panic! */
            if let Ok(rows) = &db.query(r#"SELECT jo.prenom, jo.nom, jo.mail, pa.fanion, cat.sport
                                        FROM joueur jo 
                                        JOIN pays pa 
                                        ON pa.code = jo.id_pays 
                                        JOIN categorie cat 
                                        ON cat.id_categorie = jo.id_categorie 
                                        WHERE id = $1"#, &[&id]) {
                 for row in rows {
                    let joueur = Joueur {
                        name: row.get(0),
                        lastname: row.get(1),
                        mail: row.get(2),
                        flag: row.get(3),
                        sport: row.get(4),
                    };
                    out.push(joueur);
                }
            }
            Response::json(&out)
        },

        (GET) (/api/sport/{id: i32}) => {

            /* Nouveau vecteur */
            let mut out = Vec::new();
            /* Requete utilisé pour itérer sur tout les lignes de la tables */
            if let Ok(rows) = &db.query("SELECT eq.id_categorie, eq.sport
                                        FROM categorie 
                                        WHERE eq.id_categorie = $1", &[&id]) {
                for row in rows {
                    let equipe = Sport {
                        id:row.get(0),
                        sport: row.get(1) };
                    out.push(equipe);
                }
            }

            Response::json(&out)
        },

        /* PUT = UPDATE */
        (PUT) (/modifier_joueur/{id: i32}) => {
            /* Cette route modifie l'info du joueur */

            /* Nous démmarrons par lire le corps d'une requete HTTP dans une str */
            let body = try_or_400!(rouille::input::plain_text_body(&request));

            /* Cette requete écrit les infos dans `content` si la requete SQL est mal faites = panic! */
            let updated = db.execute("UPDATE player SET pays = $2 WHERE id = $1",
                                     &[&id, &body]).unwrap();

            /* message d'erreur si joueur existe pas */
            if updated >= 1 {
                Response::text("Le joueur a bien été modifié")
            } else {
                Response::empty_404()
            }
        },

        /* POST = INSERT */
        (POST) (/api/sport/add) => {
            /* Cette route insert un nouveau sport */

            let data = try_or_400!(post_input!(&request, {
                sport: String,
            }));
            let mut id: Option<i32> = None;
             /* Cette requete écrit les infos dans `content` si la requete SQL est mal faites = panic! */
            for row in &db.query("INSERT INTO categorie(sport) 
                                VALUES ($1) 
                                RETURNING id_categorie", &[&data.sport]).unwrap() {

                id = Some(row.get(0));
            }
            let id = id.unwrap();

            let mut response = Response::text("Le sport a bien été créer");
            response.status_code = 201;
            response.headers.push(("Location".into(), format!("/api/sport/{}", id).into()));
            response

        },

        (DELETE) (/supprimerjoueur/{id: i32}) => {
            /* Cette route supprime le joueur, peut etre panic si SQL mal faites  */
            db.execute("DELETE FROM joueur WHERE id = $1", &[&id]).unwrap();
            Response::text("Le Joueur a bien été supprimé")
        },
        (DELETE) (/api/sport/del) => {
            /* Cette route supprime le joueur, peut etre panic si SQL mal faites  */
            let data = try_or_400!(post_input!(&request, {
                id: i32,
            }));
            db.execute("DELETE FROM categorie WHERE id_categorie = $1", &[&data.id]).unwrap();
            Response::text("Le Sport a bien été supprimé")
        },
        /* si aucune requetes ne matche pas par defaut retourne erreur 404 */
        _ => Response::empty_404()
    )
}
