




/* function func1(val) {
    let api_req;
    switch (val) {
        case 'sports':
            api_req = "/api/sports";
            break;
        case 'joueurs':
            api_req = "/api/joueurs";
            break;
        case 'equipes':
            api_req = "/api/equipes";
            break;
        default:
            console.log(val);
    }
    let jsondata = 
    jsondata;
} */


//let myJsonData = func1();
/* Construits le tableau en HTML */
function buildHtmlTable(selector) {
    var myJsonData;
    var url;
    switch (selector) {
        case '.tab-equipes':
            url = "/api/equipes";
            break;
        case '.tab-joueurs':
            url = "/api/joueurs";
            break;
        case '.tab-sports':
            url = "/api/sports";
            break;
        default:
            alert("Attention aucune requete Api dans l'url");
    }
    /* $.getJSON("/api/joueurs", function (data) {
        myJsonData = data;
        console.log(myJsonData);

    }) */
    fetch(url)
        .then(function (response) {
            return response.json();
        })
        .then(function (json) {
            myJsonData = json
            //console.log(myJsonData);
            var columns = addAllColumnHeaders(myJsonData, selector);

            for (var i = 0; i < myJsonData.length; i++) {
                var row$ = $('<tr/>');
                for (var colIndex = 0; colIndex < columns.length; colIndex++) {
                    var cellValue = myJsonData[i][columns[colIndex]];
                    if (cellValue == null) cellValue = "";
                    row$.append($('<td/>').html(cellValue));
                }
                $(selector).append(row$);
            }
        });
}

/* Ajoute les lignes du tableau et de l'entete */
// Need to do union of keys from all records as some records may not contain
// all records.
function addAllColumnHeaders(myJsonData, selector) {
    var columnSet = [];
    var headerTr$ = $('<tr/>');

    for (var i = 0; i < myJsonData.length; i++) {
        var rowHash = myJsonData[i];
        for (var key in rowHash) {
            if ($.inArray(key, columnSet) == -1) {
                columnSet.push(key);
                //headerTr$.append($('<th/>').html(key));
            }
        }
    }
    $(selector).append(headerTr$);

    return columnSet;
}


window.addEventListener('DOMContentLoaded', event => {

    // Toggle the side navigation
    const sidebarToggle = document.body.querySelector('#sidebarToggle');
    if (sidebarToggle) {
        // Uncomment Below to persist sidebar toggle between refreshes
        // if (localStorage.getItem('sb|sidebar-toggle') === 'true') {
        //     document.body.classList.toggle('sb-sidenav-toggled');
        // }
        sidebarToggle.addEventListener('click', event => {
            event.preventDefault();
            document.body.classList.toggle('sb-sidenav-toggled');
            localStorage.setItem('sb|sidebar-toggle', document.body.classList.contains('sb-sidenav-toggled'));
        });
    }

});
