window.addEventListener('DOMContentLoaded', event => {
    // Simple-DataTables
    // https://github.com/fiduswriter/Simple-DataTables/wiki

    const datatablesSimple = document.getElementById('datatablesSimple');
    if (datatablesSimple) {
        new simpleDatatables.DataTable(datatablesSimple, {
            sortable: true,
            searchable: true,
            fixedColumns: false,
            fixedHeight: false,
            header: true,
            hiddenHeader: false,
            labels: {
                placeholder: "Search...", // The search input placeholder
                perPage: "{select} entries per page", // per-page dropdown label
                noRows: "No entries found", // Message shown when there are no records to show
                noResults: "No results match your search query", // Message shown when there are no search results
                info: "Showing {start} to {end} of {rows} entries" //
            },
        });
    }
});
